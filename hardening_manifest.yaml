---
apiVersion: v1

# The repository name in registry1, excluding /ironbank/
name: "opensource/python/python-pipenv/python37-pipenv"

# List of tags to push for the repository in registry1
# The most specific version should be the first tag and will be shown
# on ironbank.dsop.io
tags:
- "v2022.1.8"
- "latest"

# Build args passed to Dockerfile ARGs
args:
  BASE_IMAGE: "opensource/python/python37"
  BASE_TAG: "3.7"

# Docker image labels
labels:
  org.opencontainers.image.title: "Python37 - Pipenv"
  ## Human-readable description of the software packaged in the image
  org.opencontainers.image.description: "Pipenv is a tool that aims to bring the best of all packaging worlds (bundler, composer, npm, cargo, yarn, etc.) to the Python world. Windows is a first-class citizen, in our world. This container has Python 3.7"
  ## License(s) under which contained software is distributed
  org.opencontainers.image.licenses: "MIT License"
  ## URL to find more information on the image
  org.opencontainers.image.url: "https://hub.docker.com/r/kennethreitz/pipenv/tags"
  ## Name of the distributing entity, organization or individual
  org.opencontainers.image.vendor: "pypa"
  org.opencontainers.image.version: "v2022.1.8"
  ## Keywords to help with search (ex. "cicd,gitops,golang")
  mil.dso.ironbank.image.keywords: "packaging,pip,installer"
  ## This value can be "opensource" or "commercial"
  mil.dso.ironbank.image.type: "opensource"
  ## Product the image belongs to for grouping multiple images
  mil.dso.ironbank.product.name: "Python-Pipenv"

# List of resources to make available to the offline build context
resources:
- filename: pipenv-2022.1.8-py2.py3-none-any.whl 
  url: https://files.pythonhosted.org/packages/94/6c/c0a2f56a628d1258ac0175647ab18ee73fa0870dc0fcaa33e9be079e5fb0/pipenv-2022.1.8-py2.py3-none-any.whl
  validation:
    type: sha256
    value: 3b80b4512934b9d8e8ce12c988394642ff96bb697680e5b092e59af503178327

- filename: virtualenv-20.13.0-py2.py3-none-any.whl 
  url: https://files.pythonhosted.org/packages/ef/a1/4e1700f25211b3851e6be6675061e0c8eae7585d80177a40e9b02d1105d8/virtualenv-20.13.0-py2.py3-none-any.whl
  validation: 
    type: sha256
    value: 339f16c4a86b44240ba7223d0f93a7887c3ca04b5f9c8129da7958447d079b09

- filename: certifi-2021.10.8-py2.py3-none-any.whl
  url: https://files.pythonhosted.org/packages/37/45/946c02767aabb873146011e665728b680884cd8fe70dde973c640e45b775/certifi-2021.10.8-py2.py3-none-any.whl
  validation:
    type: sha256
    value: d62a0163eb4c2344ac042ab2bdf75399a71a2d8c7d47eac2e2ee91b9d6339569

- filename: six-1.16.0-py2.py3-none-any.whl
  url: https://files.pythonhosted.org/packages/d9/5a/e7c31adbe875f2abbb91bd84cf2dc52d792b5a01506781dbcf25c91daf11/six-1.16.0-py2.py3-none-any.whl
  validation:
    type: sha256
    value: 8abb2f1d86890a2dfb989f9a77cfcfd3e47c2a354b01111771326f8aa26e0254

- filename: distlib-0.3.4-py2.py3-none-any.whl
  url: https://files.pythonhosted.org/packages/ac/a3/8ee4f54d5f12e16eeeda6b7df3dfdbda24e6cc572c86ff959a4ce110391b/distlib-0.3.4-py2.py3-none-any.whl
  validation:
    type: sha256
    value: 6564fe0a8f51e734df6333d08b8b94d4ea8ee6b99b5ed50613f731fd4089f34b

- filename: platformdirs-2.4.1-py3-none-any.whl
  url: https://files.pythonhosted.org/packages/9f/3d/4606ee54e0af98aa8f9a672b5acfd69318a5917fbb9f8e2c3aaf9c2f293f/platformdirs-2.4.1-py3-none-any.whl
  validation:
    type: sha256
    value: 1d7385c7db91728b83efd0ca99a5afb296cab9d0ed8313a45ed8ba17967ecfca

- filename: filelock-3.4.2-py3-none-any.whl
  url: https://files.pythonhosted.org/packages/ca/6c/ab2f79146ca546875e2a633d21e968acc17042ce88e4413b340541f0d4c5/filelock-3.4.2-py3-none-any.whl
  validation:
    type: sha256
    value: cf0fc6a2f8d26bd900f19bf33915ca70ba4dd8c56903eeb14e1e7a2fd7590146

- filename: typing_extensions-4.0.1-py3-none-any.whl 
  url: https://files.pythonhosted.org/packages/05/e4/baf0031e39cf545f0c9edd5b1a2ea12609b7fcba2d58e118b11753d68cf0/typing_extensions-4.0.1-py3-none-any.whl
  validation:
    type: sha256
    value: 7f001e5ac290a0c0401508864c7ec868be4e701886d5b573a9528ed3973d9d3b

- filename: zipp-3.7.0-py3-none-any.whl
  url: https://files.pythonhosted.org/packages/52/c5/df7953fe6065185af5956265e3b16f13c2826c2b1ba23d43154f3af453bc/zipp-3.7.0-py3-none-any.whl
  validation:
    type: sha256
    value: b47250dd24f92b7dd6a0a8fc5244da14608f3ca90a5efcd37a3b1642fac9a375

- filename: virtualenv_clone-0.5.7-py3-none-any.whl
  url: https://files.pythonhosted.org/packages/21/ac/e07058dc5a6c1b97f751d24f20d4b0ec14d735d77f4a1f78c471d6d13a43/virtualenv_clone-0.5.7-py3-none-any.whl
  validation:
    type: sha256
    value: 44d5263bceed0bac3e1424d64f798095233b64def1c5689afa43dc3223caf5b0

- filename: importlib_metadata-4.10.1-py3-none-any.whl
  url: https://files.pythonhosted.org/packages/58/fd/f82049e9b21773188a72bd1bc53530b900392808f8b823e4c778e7c035f0/importlib_metadata-4.10.1-py3-none-any.whl
  validation:
    type: sha256
    value: 899e2a40a8c4a1aec681feef45733de8a6c58f3f6a0dbed2eb6574b4387a77b6

# List of project maintainers
maintainers:
- name: "Andrew Knieriem"
  username: "aknieriem"
  email: "aknieriem@ascendintegrated.com"
  cht_member: true
